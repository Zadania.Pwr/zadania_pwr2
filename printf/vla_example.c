#include <stdio.h>
#include <stdlib.h>

static void vla_rand(const size_t nr_of_elements)
{
    int vla[nr_of_elements];
    /* 
     * 1) Are we able to use initializer list?
     * 2) Are we able to get sizeof our VLA? For example if we want to zeros array by memset.
     */

    for (size_t i = 0; i < nr_of_elements; ++i)
    {
        vla[i] = rand() % 100;
    }

    for (size_t i = 0; i < nr_of_elements; ++i)
    {
        printf("%d ", vla[i]);
    }

    printf("\n");
}

int main(void)
{
    vla_rand(5);
    vla_rand(10);
    vla_rand(15);

    return 0;
}
