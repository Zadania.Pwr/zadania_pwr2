#include <stdio.h>
#include <stddef.h>
#include <stdarg.h>

static size_t va_args_example(size_t nr_of_args, ...)
{
    va_list args;
    va_start(args, nr_of_args);

    size_t sum = 0;

    for (size_t i = 0; i < nr_of_args; ++i)
    {
        sum += va_arg(args, size_t);
    }

    va_end(args);

    return sum;
}

int main(void)
{
    printf("sum = %zu\n", va_args_example(5, 1, 11, 21, 31, 41));

    return 0;
}
