#include <stdio.h>
#include <unistd.h>

/*
 * How to split integer to char array?
 *
 * const int var = 21072021;
 * const char array[] = { '2', '1', '0', '7', '2', '0', '2', '1' };
 *
 */

int main(void)
{
    enum { STDOUT = 0 };

    const int var = 21072021;
    write(STDOUT, &var, sizeof(var));
    printf("\n");

    const char array[] = { '2', '1', '0', '7', '2', '0', '2', '1' };
    write(STDOUT, &array[0], sizeof(array));
    printf("\n");

    return 0;
}














/* 
 * For example we have number equals 153.
 *
 * 1)                   Our char array:
 *      153 % 10 = 3;   <-------------- 3
 *      153 / 10 = 15;
 * 
 * 2)
 *      15 % 10 = 5;    <-------------- 5
 *      15 / 10 = 1;
 * 
 * 3)   
 *      1  % 10 = 1;    <-------------- 1
 *      1  / 10 = 0;
 * 
 * Remember about ASCII table!
 * 
 * So if we use above '%' and '/' operations we will have array like this:
 * [ '3', '5', '1' ] 
 * 
 * We need to reverse array!
 * - https://github.com/DuserSW/DArrayRaw/blob/main/src/darray_raw.c (line 1324)
 */



