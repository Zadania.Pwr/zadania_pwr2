#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>

/*
 * TODO: doxygen
 */
static int my_printf(const char* format_p, ...);

/*
 * TODO: doxygen
 */
static size_t convert_int_to_byte_array(const int number, char buffer[]);

static int my_printf(const char* format_p, ...)
{
    /* Think about check if number of '%' equals LENGTH(...)! */

    if (format_p == NULL)
    {
        return -1;
    }

    const size_t format_length = strlen(format_p);

    if (format_length == 0)
    {
        return -1;
    }

    va_list va_args;
    va_start(va_args, format_p);

    size_t buffer_inserted_elements = 0;
    const size_t buffer_length = 128;

    /* Think about buffer overflow or too many write calls! */
    char buffer[buffer_length];
    (void)memset(&buffer[0], 0, sizeof(buffer));

    for (size_t i = 0; i < format_length; ++i)
    {
        /* TODO: do not forget about corner case like: printf("%%"); */
        if (format_p[i] == '%')
        {
            i += 1;

            switch (format_p[i])
            {
                case '%':
                {
                    buffer[buffer_inserted_elements] = '%';
                    buffer_inserted_elements++;
                    break;
                }

                case 'c':
                {
                    /* second argument to 'va_arg' is of promotable type 'char'; 
                     * this va_arg has undefined behavior because arguments will be promoted to 'int' */
                    const char val = (char)va_arg(va_args, int);
                    buffer[buffer_inserted_elements] = val;
                    buffer_inserted_elements += 1;
                    break;
                }

                case 'd':
                {
                    const int val = (int)va_arg(va_args, int);
                    buffer_inserted_elements += convert_int_to_byte_array(val, &buffer[buffer_inserted_elements]);
                    break;
                }

                default:
                    break;
            }
        }
        else
        {
            buffer[buffer_inserted_elements] = format_p[i];
            buffer_inserted_elements += 1;
        }
    }

    va_end(va_args);

    enum { STDOUT = 0 };
    return (int)write(STDOUT, &buffer[0], buffer_inserted_elements);
}

static size_t convert_int_to_byte_array(const int number, char buffer[])
{
    // STEP1: calculate how many characters will we have including 'minus'
    int copy_number = number;
    size_t internal_buffer_size = 0;

    if (number < 0)
    {
        internal_buffer_size++;
        copy_number *= -1;
    }

    while (copy_number > 0)
    {
        internal_buffer_size++;
        copy_number /= 10;
    }

    // STEP2: create buffer by using VLA feature
    char internal_buffer[internal_buffer_size];

    // STEP3: assign once again original number
    if (number < 0)
    {
        copy_number = number * -1;
    }
    else
    {
        copy_number = number;
    }

    /* STEP4: Save digits for our internal buffer
     *        Digits got from any number will be in reverse order (modulo operation)
     *
     * Example:
     *          From 125 we want: '1', '2', '5' but
     * 
     *          1) 125 % 10 = 5; 125 / 10 = 12
     *          2) 12  % 10 = 2; 12  / 10 = 1;
     *          3) 1   % 10 = 1; 1   / 10 = 0;
     * 
     *          In internal buffer we will have ['5', '2', '1']
     */
    size_t j = 0;

    while (copy_number > 0)
    {
        const char digit = (char)(copy_number % 10);

        // We need to add 48 because of '0' start in 48 position in ASCII table.
        internal_buffer[j] = digit + 48;
        j++;

        copy_number /= 10;
    }

    if (number < 0)
    {
        internal_buffer[internal_buffer_size - 1] = '-';
    }

    // assign from VLA to buffer
    for (size_t i = 0; i < internal_buffer_size; ++i)
    {
        buffer[i] = internal_buffer[internal_buffer_size - i - 1];
    }

    return internal_buffer_size;
}

int main(void)
{
    my_printf("Example: %c %d %d\n", 'C', 99, -101);
    return 0;
}
