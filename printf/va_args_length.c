#include <stdio.h>
#include <stdarg.h>

#define VA_ARGS_LENGTH_PRIV(_1, _2, _3, _4, _5, _N, ...) _N

#define VA_ARGS_LENGTH(...) VA_ARGS_LENGTH_PRIV(__VA_ARGS__, 5, 4, 3, 2, 1)

/*
 * We have called -> VA_ARGS_LENGTH(1, 2, 3)
 *
 * As a result, next macro looks like -> VA_ARGS_LENGTH_PRIV( [1, 2, 3], 5, 4, 3, 2, 1)
 *                                                           __VA_ARGS__
 * 
 * VA_ARGS_LENGTH_PRIV will get this:
 *  1,  2,  3,  5,  4,  3,  2, 1
 *  |   |   |   |   |   |    |
 * _1, _2, _3, _4, _5, _N,  ...
 * 
 * Finally first macro looks like:
 * 
 * VA_ARGS_LENGTH(1, 2, 3) (3)
 * 
 * How nargs.h should look like:
 * - https://github.com/DuserSW/DArrayRaw/blob/main/inc/darray_raw/nargs.h
 * 
 * Better example of usage function: darray_raw_create_and_init
 * - https://github.com/DuserSW/DArrayRaw/blob/main/inc/darray_raw/darray_raw.h
 * - https://github.com/DuserSW/DArrayRaw/blob/main/src/darray_raw.c
 * - https://github.com/DuserSW/DArrayRaw/blob/main/test/darray_raw_test.c
 */

int main(void)
{
    printf("length = %d\n", VA_ARGS_LENGTH(1, 2, 3));
}
