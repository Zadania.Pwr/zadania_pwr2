#include <stdio.h>
#include <string.h>
#include <stdint.h>


#define swap(first_addr_p, second_addr_p, size_of) \
    do { \
        uint8_t vla[size_of]; \
        memcpy(&vla[0], first_addr_p, size_of); \
        memcpy(first_addr_p, second_addr_p, size_of); \
        memcpy(second_addr_p, &vla[0], size_of); \
    } while (0)


int main(void)
{
    int a = 17;
    int b = 31;

    printf("a = %d\tb = %d\n", a, b);

    swap(&a, &b, sizeof(a));
    printf("a = %d\tb = %d\n", a, b);

    return 0;
}
