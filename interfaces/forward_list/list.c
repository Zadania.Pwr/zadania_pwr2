#include <stdio.h>
#include <stdlib.h>
#include "list.h"

void listInitialize(List * list, int (*compare)(void*, void*), void (*copy)(void*, void*, size_t), size_t elem_size){

    *list = (List) malloc(sizeof(struct ListInfo));
    (*list)->firstRec = NULL;
    (*list)->compare = compare;
    (*list)->copy = copy;
    (*list)->elem_size = elem_size;

}

int listEmpty(List list){

    return list->firstRec == NULL;

}


void listInsert(List * list, void * elem){

    size_t elem_size = (*list)->elem_size;
    listType new_node = (listType)malloc(sizeof(struct ListRec));
    new_node->storage = malloc(elem_size);
    (*list)->copy(new_node->storage, elem, elem_size);
    new_node->next = NULL;

    

    if (listEmpty(*list)){

        (*list)->firstRec = new_node;

    }else{

        listType runner = (*list)->firstRec;

        while (runner->next != NULL){

            runner = runner->next;
        }

        runner->next = new_node;


    }


}

void * listFind(List * list, void* elem){ // potential problem here


    if (listEmpty(*list)) return NULL;

    listType runner = (*list)->firstRec;

    while(runner != NULL){
        
        if ((*list)->compare(runner->storage, elem) == 0){

            return runner->storage;
        }
        runner = runner->next;

    }

    return NULL;

}

void listRemove(List * list, void * elem){


    if (listEmpty(*list)) return;

    listType runner = (*list)->firstRec;
    listType prev = NULL;

    int found = 0;

    

    while (runner != NULL && !found){

        if ((*list)->compare(runner->storage, elem) == 0){

            found = 1;
            break;
        }
        prev = runner;
        runner = runner->next;

    }

    if (prev != NULL){

        prev->next = runner->next;
        free(runner->storage);
        free(runner);

       

    }else{

        (*list)->firstRec = (*list)->firstRec->next;
        free(runner->storage);
        free(runner);

    }


}

void listDestroy(List * list){

    while (!listEmpty(*list)){

        listType tmp = (*list)->firstRec;
        (*list)->firstRec = (*list)->firstRec->next;
        free(tmp->storage);
        free(tmp);

    }

    free(*list);

}

void listTraverse(List list, void (*visit)(void*)){

    if(listEmpty(list)) return;
    listType runner = list->firstRec;
    while (runner != NULL){

        visit(runner->storage);

        runner = runner->next;

    }

}

List reverseList(List list){

    return NULL;

}

List copyList(List list){

    if (listEmpty(list)) return NULL;
    List newList;
    listInitialize(&newList, list->compare, list->copy, list->elem_size);
    listType runner = list->firstRec;

    while (runner != NULL){

        listInsert(&newList, runner->storage);
        runner = runner->next;
    }

    return newList;

}

List listReverse(List list){

    if (listEmpty(list)) return NULL;
    
    List newList = copyList(list);

    if (newList->firstRec->next == NULL) return newList;

    listType p = newList->firstRec->next;
    listType q = newList->firstRec;
    listType newHead = newList->firstRec;

    while(p != NULL){

        if (q == newList->firstRec) 
            q->next = NULL;
        else
            q->next = newHead;

        newHead = q;
        q = p;
        p = p->next;


    }

    q->next = newHead;
    newHead = q;

    newList->firstRec = newHead;

    return newList;
    


}

int listCount(List list){

    if (listEmpty(list)) return 0;

    listType runner = list->firstRec;
    int res = 0;
    while (runner != NULL){

        ++res;
        runner = runner->next;
    }

    return res;

}

int listCompare(List l1, List l2){

    int size1 = listCount(l1);
    int size2 = listCount(l2);

    if (size1 != size2){

        return 0;

    }else{

        listType runner1 = l1->firstRec;
        listType runner2 = l2->firstRec;

        while (runner1 != NULL){

            if (l1->compare(runner1->storage, runner2->storage) != 0) return 0;
            
            runner1 = runner1->next;
            runner2 = runner2->next;


        }
        return 1;
    }


}
