#include <stdio.h>
#include <stdlib.h>

#include "../interfaces/hash_table/hash_table.h"
#include "../interfaces/dynamic_array/array.h"


//runs in quadratic time
int * sumOfTwoNaive(int * inputArray, int targetSum){


    int *result = NULL;

    int inputArrayLength = array_length(inputArray);

    for (int i = 0; i < inputArrayLength; ++i){

        for (int j = 1; j < inputArrayLength; ++j){

            if (inputArray[i] != inputArray[j]){

                    int sum = inputArray[i] + inputArray[j];

                    if (sum == targetSum){
                    array_push(result, inputArray[i]);
                    array_push(result, inputArray[j]);
                    return result;
                }

            }

        }

    }

    return result;

}




int main (int argc, char * argv[]){

  return EXIT_SUCCESS;

}

