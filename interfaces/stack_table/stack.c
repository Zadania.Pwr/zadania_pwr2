#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stack.h"


void stackInitialize(StackType * stack){

  //printf("Stack initialize\n");
  (*stack) = malloc(sizeof(struct StackRec));
  (*stack)->firstIndex = (*stack)->lastIndex = 0;  

}

int stackEmpty(StackType stack){
  //printf("is stack empty...\n");
  return stack->firstIndex == stack->lastIndex;

}


void * stackTop(StackType stack){
  
  if (stackEmpty(stack)) return NULL;
  
  //printf("get value from stack %i\n", stack->lastIndex);
  return (stack->storage[stack->lastIndex - 1]);
  
}

int stackPush(void * el, size_t nbrOfBytes, StackType * stack){
    
    //printf("push value to stack\n");
    void * elem = malloc(nbrOfBytes);

    memcpy(elem, el, nbrOfBytes);

    (*stack)->storage[(*stack)->lastIndex] = elem;
    (*stack)->lastIndex++;
        
    return STACK_OK;

}

void stackPop(StackType * stack){

    //printf("Pop from stack\n");
    if (stackEmpty(*stack)) return;
    void * elem = (*stack)->storage[(*stack)->lastIndex - 1];
    free(elem);
    (*stack)->lastIndex--; 
   
}

void stackDestroy(StackType * stack){
  
    //printf("Destroy stack\n");
    if (stackEmpty(*stack)){
        free(*stack);
    }else{

        while(!stackEmpty(*stack)){
            stackPop(stack);
        }
        free(*stack);
    }

}
